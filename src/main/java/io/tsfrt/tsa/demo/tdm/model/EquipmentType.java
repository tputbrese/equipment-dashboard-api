package io.tsfrt.tsa.demo.tdm.model;

import javax.persistence.*;

@Entity
@Table(name = "EQUIPMENT_TYPE", schema = "TSA_DEMO")
public class EquipmentType {

    
    private Long id;
    private String equipmentType;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EQPT_TYPE_SEQ")
    @SequenceGenerator(sequenceName = "eqpt_type_Sequence", allocationSize = 1, name = "EQPT_TYPE_SEQ")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
