package io.tsfrt.tsa.demo.tdm.model;

import javax.persistence.*;
//making a change again, again
@Entity
@Table(name = "CONFIGURATION", schema = "TSA_DEMO")
public class Configuration {
    
    private Long id;
    private String securityCode;
    private String algorithm;
    private Double sensitivity;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONFIG_SEQ")
    @SequenceGenerator(sequenceName = "config_Sequence", allocationSize = 1, name = "CONFIG_SEQ")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public Double getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(Double sensitivity) {
        this.sensitivity = sensitivity;
    }

}
