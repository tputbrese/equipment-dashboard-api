
package io.tsfrt.tsa.demo.tdm.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.tsfrt.tsa.demo.tdm.model.Equipment;
import io.tsfrt.tsa.demo.tdm.repo.EquipmentRepo;

@RestController
public class EquipmentController {

    Logger logger = LoggerFactory.getLogger(EquipmentController.class);

    @Autowired
    EquipmentRepo eqptRepo;

    @GetMapping("/v1/equipment")
    public Iterable<Equipment> getAllEqpt() {
        return eqptRepo.findAll();
    }

    @GetMapping("/v1/equipment/{id}")
    public Equipment getEqptById(@PathVariable Long id) {
        return eqptRepo.findById(id).get();
    }



}
