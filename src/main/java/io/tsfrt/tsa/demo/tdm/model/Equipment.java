package io.tsfrt.tsa.demo.tdm.model;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "EQUIPMENT", schema = "TSA_DEMO")
@Component
public class Equipment {
    Logger logger = LoggerFactory.getLogger(Equipment.class);

    private Long id;
    private String serialNumber;
    private Configuration configuration;
    private EquipmentType equipmentType;

    @Value("${CF_INSTANCE_INTERNAL_IP:Unknown}")
    private String ipAddress;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EQPT_SEQ")
    @SequenceGenerator(sequenceName = "eqpt_Sequence", allocationSize = 1, name = "EQPT_SEQ")
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = "SERIAL_NUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    

    @OneToOne()
    @JoinColumn(name = "ID")
    public Configuration getConfigurationId() {
        return configuration;
    }

    public void setConfigurationId(final Configuration configurationId) {
        this.configuration = configurationId;
    }

    @OneToOne()
    @JoinColumn(name = "ID")
    public EquipmentType getEquipmentTypeId() {
        return equipmentType;
    }

    public void setEquipmentTypeId(final EquipmentType equipmentTypeId) {
        this.equipmentType = equipmentTypeId;
    }

    @OneToOne()
    @JoinColumn(name = "ID")
    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(final Configuration configuration) {
        this.configuration = configuration;
    }

    @OneToOne()
    @JoinColumn(name = "ID")
    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(final EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    @Transient
    public String getIpAddress() throws SocketException {
        List<String> array = new ArrayList<>();
        Enumeration<NetworkInterface> net = NetworkInterface.getNetworkInterfaces();
        while (net.hasMoreElements()) {
            NetworkInterface ni = net.nextElement();
            Enumeration<InetAddress> inetAddress = ni.getInetAddresses();
            InetAddress currentAddress;
            currentAddress = inetAddress.nextElement();
            while (inetAddress.hasMoreElements()) {
                currentAddress = inetAddress.nextElement();

                array.add(currentAddress.toString());
                break;

            }
        }

        array.add(System.getenv("CF_INSTANCE_IP"));

        array.add(System.getenv("CF_INSTANCE_INTERNAL_IP"));
        return String.join(",", array);

    }

}
