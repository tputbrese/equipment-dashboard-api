package io.tsfrt.tsa.demo.tdm.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.tsfrt.tsa.demo.tdm.model.Equipment;

@Repository
public interface EquipmentRepo extends CrudRepository<Equipment, Long> {

    public Optional<Equipment> findById(Long id);
}