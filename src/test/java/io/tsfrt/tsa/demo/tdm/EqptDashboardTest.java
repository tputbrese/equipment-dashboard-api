package io.tsfrt.tsa.demo.tdm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

import io.tsfrt.tsa.demo.tdm.controller.DemoController;
import io.tsfrt.tsa.demo.tdm.controller.EquipmentController;

@RunWith(SpringRunner.class)
@SpringBootTest()
@Profile(value = "h2")
public class EqptDashboardTest {

    @Autowired
    private EquipmentController eqptController;

    @Autowired
    DemoController demoController;

    @Test
    public void getApiConfigTest() {
        assert(true);
        assert (eqptController != null && demoController.getConfig() != null);
    
    }

    @Test
    public void getConnectConfig() {
        assert (demoController.getConnectConfig() != null);
    }
}